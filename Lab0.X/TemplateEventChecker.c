/*
 * File:   Lab0EventChecker.c
 * Author: Gabriel Hugh Elkaim
 *
 * Template file to set up typical EventCheckers for the  Events and Services
 * Framework (ES_Framework) on the Uno32 for the CMPE-118/L class. Note that
 * this file will need to be modified to fit your exact needs, and most of the
 * names will have to be changed to match your code.
 *
 * This EventCheckers file will work with both FSM's and HSM's.
 *
 * Remember that EventCheckers should only return TRUE when an event has occured,
 * and that the event is a TRANSITION between two detectable differences. They
 * should also be atomic and run as fast as possible for good results.
 *
 * This file includes a test harness that will run the event detectors listed in the
 * ES_Configure file in the project, and will conditionally compile main if the macro
 * EVENTCHECKER_TEST is defined (either in the project or in the file). This will allow
 * you to check you event detectors in their own project, and then leave them untouched
 * for your project unless you need to alter their post functions.
 *
 * Created on September 27, 2013, 8:37 AM
 */

/*******************************************************************************
 * MODULE #INCLUDE                                                             *
 ******************************************************************************/

#include "ES_Configure.h"
#include "TemplateService.h"
#include "TemplateEventChecker.h"
#include "TemplateFSM.h"
#include "ES_Events.h"
#include "serial.h"
#include "AD.h"
#include "roach.h"

/*******************************************************************************
 * MODULE #DEFINES                                                             *
 ******************************************************************************/
#define BATTERY_DISCONNECT_THRESHOLD 175
#define DARK_THRESHOLD 700
#define LIGHT_THRESHOLD 500
/*******************************************************************************
 * EVENTCHECKER_TEST SPECIFIC CODE                                                             *
 ******************************************************************************/

//#define EVENTCHECKER_TEST
#ifdef EVENTCHECKER_TEST
#include <stdio.h>
#define SaveEvent(x) do {eventName=__func__; storedEvent=x;} while (0)

static const char *eventName;
static ES_Event storedEvent;
#endif

/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/* Prototypes for private functions for this EventChecker. They should be functions
   relevant to the behavior of this particular event checker */

/*******************************************************************************
 * PRIVATE MODULE VARIABLES                                                    *
 ******************************************************************************/

/* Any private module level variable that you might need for keeping track of
   events would be placed here. Private variables should be STATIC so that they
   are limited in scope to this module. */

/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/
uint8_t TemplateCheckBattery(void) {
    static ES_EventTyp_t lastEvent = BATTERY_DISCONNECTED;
    ES_EventTyp_t curEvent;
    ES_Event thisEvent;
    uint8_t returnVal = FALSE;
    uint16_t batVoltage = AD_ReadADPin(BAT_VOLTAGE); // read the battery voltage

    if (batVoltage > BATTERY_DISCONNECT_THRESHOLD) { // is battery connected?
        curEvent = BATTERY_CONNECTED;
    } else {
        curEvent = BATTERY_DISCONNECTED;
    }
    if (curEvent != lastEvent) { // check for change from last time
        thisEvent.EventType = curEvent;
        thisEvent.EventParam = batVoltage;
        returnVal = TRUE;
        lastEvent = curEvent; // update history
#ifndef EVENTCHECKER_TEST           // keep this as is for test harness
      PostTemplateFSM(thisEvent);
#else
        SaveEvent(thisEvent);
#endif   
    }
    return (returnVal);
}

uint8_t CheckLight(void) {
    static ES_EventTyp_t lastEvent = LIGHT;
    ES_EventTyp_t curEvent = lastEvent;
    ES_Event thisEvent;
    uint8_t returnVal = FALSE;
    // Read Current Light Level; 
    uint16_t currentLightLevel = Roach_LightLevel(); 
    
    // if dark 
    if (currentLightLevel > DARK_THRESHOLD){
        curEvent = DARK; 
    }
    if(currentLightLevel < LIGHT_THRESHOLD){
        curEvent = LIGHT;
    }
    if (curEvent != lastEvent) { // check for change from last time
        thisEvent.EventType = curEvent;
        thisEvent.EventParam = currentLightLevel;
        returnVal = TRUE;
        lastEvent = curEvent; // update history
#ifndef EVENTCHECKER_TEST           // keep this as is for test harness
       PostTemplateFSM(thisEvent);
#else
        SaveEvent(thisEvent);
#endif   
    }
    return (returnVal);
}
//uint8_t CheckBumper(void) {
//    static ES_EventTyp_t lastEvent = NB;
//    ES_EventTyp_t curEvent = lastEvent;
//    ES_Event thisEvent;
//    uint8_t returnVal = FALSE;
//    // Read Current Light Level; 
//    uint16_t currentBumper = Roach_ReadBumpers(); 
//    if(currentBumper == 0b0011)
//        curEvent = FRONT;
//    if(currentBumper == 0b1100)
//        curEvent = BACK;
//    if(currentBumper == 0b0001)
//        curEvent = FL; 
//    if(currentBumper == 0b0010)
//        curEvent = FR; 
//    if(currentBumper == 0b0100)
//        curEvent = BL; 
//    if(currentBumper == 0b1000)
//        curEvent = BR;
//    if(currentBumper == 0b0000)
//        curEvent = NB;
//    if (curEvent != lastEvent) { // check for change from last time
//        thisEvent.EventType = curEvent;
//        thisEvent.EventParam = currentBumper;
//        returnVal = TRUE;
//        lastEvent = curEvent; // update history
//#ifndef EVENTCHECKER_TEST           // keep this as is for test harness
//  //     PostGenericService(thisEvent);
//#else
// //       SaveEvent(thisEvent);
//#endif   
//    }
//    return (returnVal);
//}


/* 
 * The Test Harness for the event checkers is conditionally compiled using
 * the EVENTCHECKER_TEST macro (defined either in the file or at the project level).
 * No other main() can exist within the project.
 * 
 * It requires a valid ES_Configure.h file in the project with the correct events in 
 * the enum, and the correct list of event checkers in the EVENT_CHECK_LIST.
 * 
 * The test harness will run each of your event detectors identically to the way the
 * ES_Framework will call them, and if an event is detected it will print out the function
 * name, event, and event parameter. Use this to test your event checking code and
 * ensure that it is fully functional.
 * 
 * If you are locking up the output, most likely you are generating too many events.
 * Remember that events are detectable changes, not a state in itself.
 * 
 * Once you have fully tested your event checking code, you can leave it in its own
 * project and point to it from your other projects. If the EVENTCHECKER_TEST marco is
 * defined in the project, no changes are necessary for your event checkers to work
 * with your other projects.
 */
#ifdef EVENTCHECKER_TEST
#include <stdio.h>
static uint8_t(*EventList[])(void) = {EVENT_CHECK_LIST};

void PrintEvent(void);

//void main(void) {
//    BOARD_Init();
//    Roach_Init();
//    /* user initialization code goes here */
//
//    // Do not alter anything below this line
//    int i;
//
//    printf("\r\nEvent checking test harness for %s", __FILE__);
//
//    while (1) {
//        if (IsTransmitEmpty()) {
//            for (i = 0; i< sizeof (EventList) >> 2; i++) {
//                if (EventList[i]() == TRUE) {
//                    PrintEvent();
//                    break;
//                }
//
//            }
//        }
//    }
//}

void PrintEvent(void) {
    printf("\r\nFunc: %s\tEvent: %s\tParam: 0x%X", eventName,
            EventNames[storedEvent.EventType], storedEvent.EventParam);
}
#endif